﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour {
    [SerializeField] Vector3 motion = new Vector3(0, 10, 0);
    [SerializeField] float speed = 10f;
    Vector3 startPosition;
    float accumulator = 0;
	// Use this for initialization
	void Start () {
        startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        accumulator += Time.deltaTime*speed/100f;
        transform.position = startPosition + motion * Mathf.Sin(accumulator);
	}
}
