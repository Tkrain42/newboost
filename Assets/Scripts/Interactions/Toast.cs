﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Toast : MonoBehaviour {
    [SerializeField] ScriptableString message;
    float timer = .1f;
    Text text;
	// Use this for initialization
	void Start () {
        print("Start");
        text = GetComponent<Text>();

        message.onValueChanged += onValueChanged;
	}

    void onValueChanged()
    {
        text.text = message.Value;
        print(text.text);
        timer = 1;
    }

	// Update is called once per frame
	void Update () {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                text.text = "";
            }
        }
	}

    private void OnDisable()
    {
        message.onValueChanged -= onValueChanged;
    }
}
