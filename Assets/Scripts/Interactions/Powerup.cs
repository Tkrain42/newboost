﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Powerup : MonoBehaviour {

    [SerializeField] ScriptableInt score;
    [SerializeField] ScriptableFloat statToBoost;
    [SerializeField] ScriptableString messenger;
    [SerializeField] int scoreModifier;
    [SerializeField] float statModifier;
    [SerializeField] string message;
    [SerializeField] ScriptableAudioArray clips;
    public void Collected()
    {
        score.Value += scoreModifier;
        statToBoost.Value += statModifier;
        //Todo: Broadcast message
        if (clips && clips.Count>0)
        {
            GetComponent<AudioSource>().PlayOneShot(clips.GetRandomClip());
        }
        if (messenger)
        {
            messenger.Value = message;
        }
        GetComponent<Collider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;

    }
   

}
