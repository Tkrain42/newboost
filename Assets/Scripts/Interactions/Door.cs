﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Door : MonoBehaviour {
    Animator anim;
    [SerializeField] ScriptedKey key;
    [SerializeField] ScriptableInt score;
    [SerializeField] int scoreModifier = 100;
    [SerializeField] ScriptableString messenger;
    [SerializeField] string message = "Door opened!";

    private void Start()
    {
        anim = GetComponent<Animator>();
        key.onValueChanged += onValueChanged;
    }


    void onValueChanged()
    {
        if (key.Count <= 0)
        {
            anim.SetTrigger("Open");
            score.Value += scoreModifier;
            if (messenger)
            {
                messenger.Value = message;
            }
        }
    }

    private void OnDisable()
    {
        key.onValueChanged -= onValueChanged;
    }
}
