﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisRotate : MonoBehaviour {

    [SerializeField] Vector3 rotation = Vector3.forward;

    private void Update()
    {
        transform.Rotate(rotation*Time.deltaTime);
    }
}
