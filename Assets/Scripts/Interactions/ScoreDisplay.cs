﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(Text))]
public class ScoreDisplay : MonoBehaviour {
    [SerializeField] ScriptableInt score;
    [SerializeField] string caption = "Score: ";
    Text text;
    // Use this for initialization

    private void Awake()
    {
        score.onValueChanged += OnValueChanged;
    }
    void Start () {
        text = GetComponent<Text>();
        //score.onValueChanged += OnValueChanged;
        OnValueChanged();
	}

    void OnValueChanged()
    {
        text.text = caption + score.Value;
    }

    private void OnDisable()
    {
        score.onValueChanged -= OnValueChanged;
    }

 
    private void Update()
    {
        //OnValueChanged();
    }
}
