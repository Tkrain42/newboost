﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class DoorKey : MonoBehaviour {
    [SerializeField] ScriptedKey key;
    [SerializeField] ScriptableInt score;
    [SerializeField] ScriptableAudioArray clips;
    [SerializeField] int scoreModifier = 10;
    [SerializeField] ScriptableString messenger;
    AudioSource audio;
    bool isTriggered = false;
	// Use this for initialization
	void Start () {
        key.Count += 1;
        audio = GetComponent<AudioSource>();
	}

    public void CollectKey()
    {
        if (isTriggered)
        {
            return;
        }
        if (messenger)
        {
            messenger.Value = "You collected a " + key.KeyName + " key!";
        }
        isTriggered = true;
        key.Count -= 1;
        score.Value += scoreModifier;
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        if (clips)
        {
            audio.PlayOneShot(clips.GetRandomClip());
        }
    }

    private void OnDisable()
    {
        if (!isTriggered)
        {
            key.Count -= 1;
        }
    }

}
