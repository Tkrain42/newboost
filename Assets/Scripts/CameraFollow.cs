﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    GameObject player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindObjectOfType<Thruster>().gameObject;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 pos = transform.position;
        pos.x = player.transform.position.x;
        pos.y = player.transform.position.y+5;
        transform.position = pos;
	}
}
