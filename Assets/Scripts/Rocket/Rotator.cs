﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Rigidbody))]
public class Rotator : MonoBehaviour
{

    [SerializeField] ScriptableFloat turn;
    [SerializeField] ScriptableBool alive;
    new Rigidbody rigidbody;

    Vector3 TurnLeft
    {
        get
        {
            return -Vector3.forward * turn.Value;
        }
    }
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        rigidbody.freezeRotation = true;
        transform.Rotate(TurnLeft * CrossPlatformInputManager.GetAxis("Horizontal") * Time.deltaTime);
        rigidbody.freezeRotation = false;
    }
}
