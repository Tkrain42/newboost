﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Impact : MonoBehaviour {

    [SerializeField] ScriptableFloat maxImpact;
    [SerializeField] ScriptableInt lives;
    [SerializeField] ScriptableBool alive;

	// Use this for initialization
	void Start () {
        alive.Value = true;
	}

    private void OnCollisionEnter(Collision collision)
    {
        //print(collision.gameObject.name);
        if (!alive.Value)
        {
            return;
        }
        if (collision.gameObject.tag == "Obstacle")
        {
            lives.Value -= 1;
            alive.Value = false;
            GetComponent<ExplodeShip>().onExplodeShip();
            Invoke("LoadStartScene", 5);
        }

    }

    void LoadStartScene()
    {
        if (lives.Value < 0)
        {
            SceneManager.LoadScene("Startup");
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        
    }

}
