﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ExplodeShip : MonoBehaviour
{
    [SerializeField]
    AudioClip explosionSound;
    [SerializeField]
    ParticleSystem particle;

    new AudioSource audio;
    MeshRenderer[] meshes;
    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        meshes = GetComponentsInChildren<MeshRenderer>();
    }

    public void onExplodeShip()
    {
        foreach (MeshRenderer mesh in meshes)
        {
            mesh.transform.SetParent(mesh.transform);
            Rigidbody rigidbody = mesh.gameObject.AddComponent<Rigidbody>();
            rigidbody.AddExplosionForce(1000, transform.position, 10);

        }
        audio.PlayOneShot(explosionSound);
        Instantiate(particle, transform.position, Quaternion.identity);
        GetComponent<Rigidbody>().isKinematic = true;
    }
}