﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProcessTriggers : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        Powerup powerup = other.GetComponent<Powerup>();
        if (powerup)
        {
            powerup.Collected();
        }
        DoorKey doorkey = other.GetComponent<DoorKey>();
        if (doorkey)
        {
            doorkey.CollectKey();
        }
    }
}
