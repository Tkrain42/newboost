﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class EngineParticle : MonoBehaviour {

    [SerializeField]
    ScriptableBool thrusting;
    [SerializeField]
    ScriptableBool alive;

    ParticleSystem particle;
    // Use this for initialization
    void Start () {
        particle = GetComponent<ParticleSystem>();
        thrusting.onValueChanged += onValueChanged;
        alive.onValueChanged += onValueChanged;
	}

    void onValueChanged() //This works when either alive or thrusting changes, as the only condition to play is if you're alive AND thrusting.
    {
        if (alive.Value & thrusting.Value)
        {
            particle.Play();
        }
        else
        {
            particle.Stop();
        }

    }

    private void OnDisable()
    {
        thrusting.onValueChanged -= onValueChanged;
        alive.onValueChanged -= onValueChanged;
    }
}
