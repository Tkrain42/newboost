﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Rigidbody))]
public class Thruster : MonoBehaviour {

    [SerializeField] ScriptableFloat thrust;
    [SerializeField] ScriptableBool thrusting;
    [SerializeField] ScriptableBool active;

    new Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}

    private void FixedUpdate()
    {
        if (!active.Value)
        {
            return;
        }
        if (CrossPlatformInputManager.GetButton("Jump"))
        {
            float boost = thrust.Value * Time.deltaTime;
            rigidbody.AddRelativeForce(Vector3.up * boost);
            thrusting.Value = true;
        }
        else
        {
            thrusting.Value = false;
        }
    }
}
