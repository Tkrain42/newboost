﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class EngineSound : MonoBehaviour {

    [SerializeField]
    ScriptableBool thrusting;
    [SerializeField]
    ScriptableBool alive;
    [SerializeField]
    AudioClip thrustingSound;
    AudioSource audioSource;
    

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = thrustingSound;
        audioSource.loop = true;
        thrusting.onValueChanged += onValueChanged;
        alive.onValueChanged += onValueChanged;
    }

   void onValueChanged() //This works when either alive or thrusting changes, as the only condition to play is if you're alive AND thrusting.
    {
        if (alive.Value & thrusting.Value)
        {
            PlaySound();
        }
        else
        {
            StopPlaying();
        }

    }

    void PlaySound()
    {
        audioSource.Play();
    }

    void StopPlaying()
    {
        audioSource.Stop();
    }

    private void OnDisable()
    {
        alive.onValueChanged -= onValueChanged;
        thrusting.onValueChanged -= onValueChanged;
    }
}
