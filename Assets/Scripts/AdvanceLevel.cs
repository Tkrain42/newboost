﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdvanceLevel : MonoBehaviour {

    [SerializeField] ScriptableBool active;
    [SerializeField] ScriptableInt score;
    [SerializeField] ScriptableInt lives;
    [SerializeField] ScriptableString messenger;
    [SerializeField] string next="";
    [SerializeField] string message = "Level Completed!";
    [SerializeField] int reward = 100;

    private void OnCollisionEnter(Collision collision)
    {
        if (active.Value)
        {
            var player = collision.gameObject.GetComponent<Thruster>();
            if (player && next != "")
            {
                messenger.Value = message;
                active.Value = false;
                score.Value += reward;
                lives.Value += 1;
                Invoke("LoadNextLevel", 2);
            }
        }
    }

    void LoadNextLevel()
    {
        SceneManager.LoadScene(next);
    }
}
