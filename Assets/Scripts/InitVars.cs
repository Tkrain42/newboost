﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitVars : MonoBehaviour {

    [SerializeField] ScriptableInt score;
    [SerializeField] ScriptableInt lives;
    [SerializeField] ScriptableFloat thrust;
    [SerializeField] ScriptableFloat turn;
    [SerializeField] ScriptableBool thrusting;
    [SerializeField] ScriptableBool playing;
    [SerializeField] int defaultScore = 0;
    [SerializeField] int defaultLives = 0;
    [SerializeField] float defaultThrust = 700;
    [SerializeField] float defaultTurn = 30;
    
    [SerializeField] bool forcereset;

    private void Start()
    {
        if (forcereset)
        {
            Reset();
        }
        var initVars = GameObject.FindObjectsOfType<InitVars>();
        if (initVars.Length > 1)
        {
            Destroy(gameObject);
            return;
        }
        Reset();
        DontDestroyOnLoad(gameObject);

    }

    private void Reset()
    {
        score.Value = defaultScore;
        lives.Value = defaultLives;
        thrust.Value = defaultThrust;
        turn.Value = defaultTurn;
        thrusting.Value = false;
        playing.Value = true;
    }
}
