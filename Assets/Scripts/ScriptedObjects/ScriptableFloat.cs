﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptedFloat", menuName = "Shared/Float")]
public class ScriptableFloat : ScriptableBase {

    [SerializeField] float val;
    float backup;

    public float Value
    {
        get
        {
            return val;
        }
        set
        {
            if (val != value)
            {
                val = value;
                AnnounceEvent();
            }
        }
    }

    private void OnEnable()
    {
        backup = val;
    }

    public void Reset()
    {
        Value = backup;
    }




}
