﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptedFloat", menuName = "Shared/Int")]

public class ScriptableInt : ScriptableBase
{

    [SerializeField] int val;
    int backup;

    public int Value
    {
        get
        {
            return val;
        }
        set
        {
            if (val != value)
            {
                val = value;
                AnnounceEvent();
            }
        }
    }

    private void OnEnable()
    {
        backup = val;
    }

    public void Reset()
    {
        Value = backup;
    }




}