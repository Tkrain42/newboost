﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableBase : ScriptableObject {

    public delegate void OnValueChanged();
    public event OnValueChanged onValueChanged;

    protected void AnnounceEvent()
    {
        if (onValueChanged != null)
        {
            onValueChanged();
        }
    }
}
