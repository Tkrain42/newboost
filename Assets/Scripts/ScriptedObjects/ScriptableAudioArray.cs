﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="AudioArray", menuName ="Shared/AudioArray")]
public class ScriptableAudioArray : ScriptableObject {

    [SerializeField] List<AudioClip> clips;

    public int Count
    {
        get
        {
            return clips.Count;
        }
    }

    public AudioClip GetClip(int clip)
    {
        return clips[Mathf.Clamp(clip, 0, clips.Count - 1)];
    }

    public AudioClip GetRandomClip()
    {
        return clips[Random.Range(0, clips.Count)];
    }
}
