﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Key", menuName = "Shared/Key")]
public class ScriptedKey : ScriptableBase
{

    [SerializeField] string keyName = "generic";
    public string KeyName
    {
        get
        {
            return keyName;
        }
    }
    float count = 0;
    public float Count
    {
        get
        {
            return count;
        }
        set
        {
            if (count != value)
            {
                Debug.LogWarning(keyName + " count = " + value);
                count = value;
                AnnounceEvent();
            }
        }


    }
    private void OnEnable()
    {
        count = 0;
    }
}
